from youtube.models import *
from django.utils import timezone
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials


def get_credential():
    """
    Get existing youtube data API access credential
    :return: credential
    """
    return YoutubeCredential.objects.last().credential


def get_client():
    """
    Get client for youtube data API
    :return: client
    """
    credential = get_credential()
    credentials = Credentials(**credential)
    client = build('youtube', 'v3', credentials=credentials)
    return client


def get_channel_items(channel_id, year, month, day, hour, minute, second, part='id,snippet', next_page_token=None):
    """
    Get list of items for a single channel
    :param channel_id: ID of the channel
    :param year: Year after which videos shall be fetched
    :param month: Month after which videos shall be fetched
    :param day: Day after which videos shall be fetched
    :param hour: Hour after which videos shall be fetched
    :param minute: Minute after which videos shall be fetched
    :param second: Second after which videos shall be fetched
    :param part: Which data for a specific item youtube API should return
    :param next_page_token: Token to get next pages items
    :return: Data including list of items
    """
    client = get_client()
    res = client.search().list(
        part=part,
        channelId=channel_id,
        publishedAfter=f'{year}-{month}-{day}T{hour}:{minute}:{second}Z',
        type='youtube#video',
        pageToken=next_page_token,
        maxResults=50
    ).execute()
    return res


def get_video_stat(video_id, part='id,snippet,statistics'):
    """
    Get statistics for a single video
    :param video_id: Id of the video
    :param part: Which data for a specific video youtube API should return
    :return: Video specific data
    """
    client = get_client()
    res = client.videos().list(id=video_id, part=part).execute()
    return res


def get_or_create_video(data, video_id, source):
    """
    Get or create Video object
    :param data: Youtube API response data
    :param video_id: Id of video
    :param source: VideoSource for the video (Channel)
    :return: Video object
    """
    # Check if video already exists
    vd = Video.objects.filter(video_id=video_id).first()

    if not vd:
        video_data = data.get('items')[0].get('snippet')
        # Creating video
        vd = Video.objects.create(
            video_id=video_id,
            source=source,
            title=video_data.get('title', None),
            description=str(video_data.get('description', None)),
            published_at=video_data.get('publishedAt', None),
            category_id=video_data.get('categoryId', None),
            live_broad_case_count=video_data.get('liveBroadcastContent', None),
        )

    return vd


def create_localized_info(obj, data):
    """
    Create Localization data for a video
    :param obj: Video object
    :param data: Youtube API response data
    :return: None
    """
    localized_data = data.get('items')[0].get('snippet').get('localized')
    # Creating video localization data
    vl = VideoLocalization.objects.create(
        title=localized_data.get('title', None),
        description=localized_data.get('description', None),
        default_audio_language=localized_data.get(
            'defaultAudioLanguage', None),
    )
    # Updating video object with newly created video localization object
    obj.localized = vl
    obj.save()


def create_tags(obj, data):
    """
    Create tags related to a video
    :param obj: Video object
    :param data: Youtube API response data
    :return: None
    """
    tag_data = data.get('items')[0].get('snippet').get('tags', [])
    for tag in tag_data:
        # Creating tags if not exists
        tg, created = VideoTag.objects.get_or_create(title=tag)
        # Assigning tags to the video
        obj.tags.add(tg)


def create_stat(obj, video):
    stat_data = video.get('items')[0].get('statistics')
    if stat_data:
        # Creating video statistics
        vs = VideoStatistics.objects.create(
            view_count=stat_data.get('viewCount', None),
            like_count=stat_data.get('likeCount', None),
            dislike_count=stat_data.get('dislikeCount', None),
            favourite_count=stat_data.get('favoriteCount', None),
            comment_count=stat_data.get('commentCount', None),
        )
        # Assigning video statistics to the video
        obj.statistics.add(vs)
        # Updating video updated_at to track the last time video statistics is updated
        obj.updated_at = timezone.now()
        obj.save()


def create_thumbnail(obj, data):
    """
    Create thumbnails for a video
    :param obj: Video object
    :param data: Youtube API response data
    :return:
    """
    for option, value in VideoThumbnail.TITLE_CHOICES:
        # Checking if thumbnail exists for each possible option
        thumbnail_data = data.get('items', None)[0].get(
            'snippet').get('thumbnails').get(option, None)
        if thumbnail_data:
            # Creating thumbnail for the video
            VideoThumbnail.objects.create(
                title=option,
                url=thumbnail_data.get('url', None),
                width=thumbnail_data.get('width', None),
                height=thumbnail_data.get('height', None),
                video=obj
            )
