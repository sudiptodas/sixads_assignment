from django.db import models
from oauth2client.contrib.django_util.models import CredentialsField


class YoutubeCredential(models.Model):
    credential = CredentialsField()
    created_at = models.DateTimeField(auto_now_add=True)


class VideoSource(models.Model):
    CHANNEL, PLAYLIST, YOUTUBE_USER = 'channel', 'playlist', 'user'
    INACTIVE, ACTIVE = 'inactive', 'active'

    SOURCE_TYPE_CHOICES = (
        # (PLAYLIST, 'Youtube Playlist'),
        (CHANNEL, 'Youtube Channel'),
        # (YOUTUBE_USER, 'Youtube User')
    )

    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (INACTIVE, 'Inactive'),
    )

    title = models.CharField(max_length=255, db_index=True)
    source_id = models.CharField(max_length=255, db_index=True)
    source_type = models.CharField(max_length=255, choices=SOURCE_TYPE_CHOICES, default=CHANNEL, db_index=True)
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class VideoLocalization(models.Model):
    title = models.CharField(max_length=255, null=True, db_index=True)
    description = models.TextField(null=True)
    default_audio_language = models.CharField(max_length=10, null=True)


class VideoStatistics(models.Model):
    view_count = models.BigIntegerField()
    like_count = models.BigIntegerField()
    dislike_count = models.BigIntegerField()
    favourite_count = models.IntegerField()
    comment_count = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)


class VideoTag(models.Model):
    title = models.CharField(max_length=255, db_index=True, unique=True)


class Video(models.Model):
    video_id = models.CharField(max_length=100, db_index=True, unique=True)
    source = models.ForeignKey(VideoSource, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True)
    published_at = models.DateTimeField()
    crawled_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    category_id = models.SmallIntegerField()
    live_broad_case_count = models.CharField(max_length=255)
    video_performance = models.FloatField(null=True, db_index=True)
    localized = models.ForeignKey(VideoLocalization, on_delete=models.SET_NULL, null=True)
    statistics = models.ManyToManyField(VideoStatistics)
    tags = models.ManyToManyField(VideoTag)
    first_hour_view = models.BigIntegerField(null=True)


class VideoThumbnail(models.Model):
    DEFAULT, MEDIUM, HIGH, STANDARD, MAXRES = 'default', 'medium', 'high', 'standard', 'maxres'
    TITLE_CHOICES = (
        (DEFAULT, 'Default'),
        (MEDIUM, 'Medium'),
        (HIGH, 'High'),
        (STANDARD, 'Standard'),
        (MAXRES, 'Max Res'),
    )

    title = models.CharField(max_length=20, choices=TITLE_CHOICES, db_index=True)
    url = models.CharField(max_length=255)
    width = models.IntegerField()
    height = models.IntegerField()
    video = models.ForeignKey(Video, on_delete=models.CASCADE, null=True)
