from youtube.models import *
from rest_framework import serializers


class VideoSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoSource
        fields = '__all__'


class VideoLocalizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoLocalization
        fields = '__all__'


class VideoStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoStatistics
        fields = '__all__'


class VideoTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoTag
        fields = '__all__'


class VideoThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoThumbnail
        fields = '__all__'


class VideoSerializer(serializers.ModelSerializer):
    localized = VideoLocalizationSerializer()
    statistics = VideoStatisticsSerializer(many=True)
    tags = VideoTagSerializer(many=True)
    source = VideoSourceSerializer()

    class Meta:
        model = Video
        fields = '__all__'
