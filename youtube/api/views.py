from operator import or_
from functools import reduce
from django.db.models import Q
from youtube.models import Video
from rest_framework.views import APIView
from rest_framework.response import Response
from youtube.api.serializers import VideoSerializer


class VideoAPIView(APIView):
    def get_queryset(self):
        limit = int(self.request.GET.get('limit', 10))
        offset = int(self.request.GET.get('offset', 0))
        vp_from = self.request.GET.get('vp_from', None)
        vp_to = self.request.GET.get('vp_to', None)
        tag_string = self.request.GET.get('tags', None)
        videos = Video.objects.all()

        if tag_string:
            tags = tag_string.split(',')
            query = reduce(or_, (Q(tags__title__icontains=tag) for tag in tags))
            videos = videos.filter(query)

        if vp_from:
            videos = videos.filter(video_performance__gte=float(vp_from))

        if vp_to:
            videos = videos.filter(video_performance__lte=float(vp_to))

        return videos[offset:limit + offset]

    def get(self, request):
        serializer = VideoSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)
