from django.urls import path
from youtube.api import views

urlpatterns = [
    path('videos/', views.VideoAPIView.as_view(), name='video_list_api'),
]
