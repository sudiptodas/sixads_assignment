from youtube import views
from django.urls import path, include

app_name = 'youtube'

urlpatterns = [
    path('authorize/', views.AuthorizeView.as_view(), name='authorize'),
    path('oauth2callback/', views.OAuth2CallBack.as_view(), name='oauth2callback'),
    path('sources/', views.VideoSourceListView.as_view(), name='video_sources'),
    path('sources/create/', views.VideoSourceCreateView.as_view(), name='video_source_create'),
    path('sources/update/<int:pk>/', views.VideoSourceUpdateView.as_view(), name='video_source_update'),
    path('sources/crawl/<int:pk>/', views.CrawlChannelView.as_view(), name='video_source_crawl'),
    path('api/v1/', include('youtube.api.urls'))
]
