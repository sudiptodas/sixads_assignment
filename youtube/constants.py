START_YEAR = 2020
START_MONTH = 1
START_DAY = 1
START_HOUR = 0
START_MINUTE = 0
START_SECOND = 0

OPTIONS = {
    'year': START_YEAR,
    'month': START_MONTH,
    'day': START_DAY,
    'hour': START_HOUR,
    'minute': START_MINUTE,
    'second': START_SECOND,
    'next_page_token': None
}
