from youtube.tasks import *
from django.views import View
from google_auth_oauthlib import flow
from django.shortcuts import redirect
from youtube.forms import VideoSourceForm
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from youtube.models import VideoSource, YoutubeCredential
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, CreateView, UpdateView
from oauth2client.contrib.django_util.storage import DjangoORMStorage
from core.settings import GOOGLE_OAUTH2_CLIENT_SECRETS_JSON, YOUTUBE_CALLBACK_FUNCTION, LOGIN_HINT


@method_decorator(login_required, name='dispatch')
class AuthorizeView(View):
    """
    Class for invoking Google OAuth authorization
    """

    def get(self, request):
        # Checking if a credential exists
        storage = DjangoORMStorage(
            YoutubeCredential, 'id', request.user.id, 'credential')
        credentials = storage.get()

        if credentials is None:
            flow_data = flow.Flow.from_client_secrets_file(
                GOOGLE_OAUTH2_CLIENT_SECRETS_JSON,
                scopes=['https://www.googleapis.com/auth/youtube.force-ssl']
            )

            flow_data.redirect_uri = YOUTUBE_CALLBACK_FUNCTION
            authorization_url, state = flow_data.authorization_url(
                access_type='offline',
                login_hint=LOGIN_HINT,
                include_granted_scopes='true',
                approval_prompt='force'
            )

            return redirect(authorization_url)
        return redirect('/')


@method_decorator(login_required, name='dispatch')
class OAuth2CallBack(View):
    """
    Class for handling Google OAuth callback
    """

    def get(self, request):
        response_state = request.GET.get('state', None)

        if not response_state:
            raise Exception('State not found.')

        flow_data = flow.Flow.from_client_secrets_file(
            GOOGLE_OAUTH2_CLIENT_SECRETS_JSON,
            scopes=['https://www.googleapis.com/auth/youtube.force-ssl'],
            state=response_state
        )

        flow_data.redirect_uri = YOUTUBE_CALLBACK_FUNCTION
        request_url = request.build_absolute_uri().replace('http', 'https')
        authorization_response = request_url
        flow_data.fetch_token(authorization_response=authorization_response)
        credentials = flow_data.credentials

        credentials = {
            'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes
        }

        # Creating new credential
        storage = DjangoORMStorage(
            YoutubeCredential, 'id', request.user.id, 'credential')
        storage.put(credentials)
        return redirect('/')


@method_decorator(login_required, name='dispatch')
class VideoSourceListView(ListView):
    model = VideoSource
    paginate_by = 100


@method_decorator(login_required, name='dispatch')
class VideoSourceCreateView(CreateView):
    form_class = VideoSourceForm
    model = VideoSource
    success_url = reverse_lazy('youtube:video_sources')

    def get_initial(self):
        initial = super(VideoSourceCreateView, self).get_initial()
        initial.update({
            'status': VideoSource.ACTIVE,
        })
        return initial

    def form_valid(self, form):
        return super().form_valid(form)


class VideoSourceUpdateView(UpdateView):
    form_class = VideoSourceForm
    model = VideoSource

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('youtube:video_sources')


@method_decorator(login_required, name='dispatch')
class CrawlChannelView(View):
    def get(self, request, pk):
        source = VideoSource.objects.get(pk=pk)
        crawl_channel(source.pk)

        return redirect(reverse('youtube:video_sources'))
