from django import forms
from youtube.models import VideoSource


class VideoSourceForm(forms.ModelForm):
    class Meta:
        model = VideoSource
        exclude = ['created_at', 'updated_at']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'source_id': forms.TextInput(attrs={'class': 'form-control'}),
            'source_type': forms.Select(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
            'created_by': forms.HiddenInput()
        }
