from copy import deepcopy
from utils.helpers import *
from datetime import timedelta
from youtube.constants import *
from django.db.models import Sum
from celery.schedules import crontab
from celery.task import task, periodic_task


@task
def crawl_video(video_id, source_pk):
    """
    Get data for a single video
    :param video_id: Id of video
    :param source_pk: Id of VideoSource (Channel)
    :return: None
    """
    exists = Video.objects.filter(video_id=video_id).first()

    if not exists:
        source = VideoSource.objects.get(pk=source_pk)
        data = get_video_stat(video_id=video_id)
        video = get_or_create_video(data, video_id, source)
        create_localized_info(video, data)
        create_thumbnail(video, data)
        create_stat(video, data)
        create_tags(video, data)


@task
def crawl_channel(pk):
    """
    Get data for a single channel
    :param pk: Id of VideoSource(Channel)
    :return: None
    """
    source = VideoSource.objects.get(pk=pk)
    # Setting up parameters for passing to get_channel_items function
    options = deepcopy(OPTIONS)
    options['channel_id'] = source.source_id
    # Getting API response
    res = get_channel_items(**options)

    item_list = []
    items = res.get('items')

    if items:
        item_list = item_list + items

    # Get items of next pages
    while res.get('nextPageToken', None):
        options['next_page_token'] = res.get('nextPageToken')
        res = get_channel_items(**options)
        items = res.get('items')
        item_list = item_list + items

    for item in item_list:
        video_id = item.get('id').get('videoId')

        if video_id:
            crawl_video.delay(video_id=video_id, source_pk=pk)


@periodic_task(
    run_every=(crontab(minute='*/5')),
    name="update_stat",
    ignore_result=True
)
def update_all_stat():
    """
    Update video stat
    """
    # Get those videos which have not been updated in last hour
    diff = timezone.now() - timedelta(hours=1)
    videos = Video.objects.filter(updated_at__lte=diff)

    for obj in videos:
        data = get_video_stat(video_id=obj.video_id, part='statistics')
        create_stat(obj, data)
        if obj.statistics.count() == 2:
            # For seconding time crawling, updating first hour view count and video performance
            first, second = obj.statistics.all()
            diff = second.view_count - first.view_count
            obj.first_hour_view = abs(diff)
            # Sum of videos view_count whose first hour view has been calculated
            fhv_sum = Video.objects.filter(
                video_performance__isnull=False
            ).aggregate(Sum('first_hour_view')).get('first_hour_view__sum')

            if fhv_sum:
                # Get median value for videos' first hour view
                median = fhv_sum / Video.objects.filter(video_performance__isnull=False).count()
                obj.video_performance = diff / median
            else:
                obj.video_performance = diff

            obj.save()
