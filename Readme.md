# Instruction for running the project
##### Information
Operating system : Ubuntu 19.10

Framework : Django

Database : PostgreSQL

Python Version: 3.7.6

Instructions
===
1# Install requirements
```python
pip install -r requirements
```
2# Install updated oauth2client
```python
pip install -e git://github.com/Schweigi/oauth2client.git@v4.1.3#egg=oauth2client
```
3# Install Redis
```python
sudo apt install redis-server
sudo service redis-server restart
```
4# Create a config.py file in core with this content
```python
# Youtube Data API Configurations
CLIENT_SECRETS_JSON = 'core/client_secret.json'
YOUTUBE_CALLBACK_FUNCTION = 'http://localhost:8000/oauth2callback/'
LOGIN_HINT = 'diptoislive@gmail.com'

# Celery Configurations
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'

# DB
DB_ENGINE = 'django.db.backends.postgresql_psycopg2'
DB_NAME = '<DB Name>'
DB_USER = '<User name>'
DB_PASSWORD = '<Password>'
DB_HOST = 'localhost'
DB_PORT = '5432'
```
5# Create a client_secret.json file in core with this content
```python
{
  "web": {
    "client_id": "790315118312-7634r3aanov3st0m8dlne2u95vo3nsf5.apps.googleusercontent.com",
    "project_id": "sixads-274007",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_secret": "1bvl-8nJJ2UQd8xg5Z35jICB",
    "redirect_uris": [
      "http://localhost:8000/oauth2callback/"
    ],
    "javascript_origins": [
      "http://localhost:8000"
    ]
  }
}
```
6# Migrate
```python
python manage.py migrate
```
7# Create a User
```python
python manage.py createsuperuser
python manage.py drf_create_token <username>
```
8# Run the project
```python
python manage.py runserver
celery -A core worker -l info # In different terminal
celery -A core beat -l info # In different terminal
# To run celery as daemon you've to follow the instruction provided in this link
https://docs.celeryproject.org/en/stable/userguide/daemonizing.html
```
 9# Log into system
 
 10# Create a credential for Youtube Data API (Proceed for http warning because we are using localhost)
 
 11# Go to Video Source and add as many channel as you want
 
```python
# Example
title = 'Tech With Time'
source_id = 'UC4JX40jDee_tINbkjycV4Sg'
```

12# Click the `Crawl` button for crawling data (For the limitation of Youtube Data API free tier I've scrapped data after 2020 January 1. You can change this in youtube/constants.py file)

13 # To see crawled data you can use the API
```python
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Token <token value>" http://localhost:8000/api/v1/videos/
```
14 # You can filter video API using these query params
```python
limit # Amount of items (Int)
offset # Skip items (Int)
vp_from # Video performance from (Float)
vp_to = # Video performance to (Float)
tag_string = # Comma separated tags ex. "tag1,tag2,tag3"
```
15 # Scheduler will run every 5 minutes to check if any video's updated time skipped 1 hour. If it's been 1 hour it will crawl statistics again