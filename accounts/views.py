from django.views import View
from django.urls import reverse
from accounts.forms import LoginForm
from django.shortcuts import redirect
from youtube.models import YoutubeCredential
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views.generic.base import TemplateResponseMixin
from oauth2client.contrib.django_util.storage import DjangoORMStorage


@method_decorator(login_required, name='dispatch')
class Dashboard(View, TemplateResponseMixin):
    """
    Class for handling dashboard view
    """
    template_name = 'dashboard.html'

    def get(self, request):
        # Get Google OAuth credential for accessing Youtube Data API
        storage = DjangoORMStorage(
            YoutubeCredential, 'id', request.user.id, 'credential'
        )
        credentials = storage.get()
        return self.render_to_response({'credentials': credentials})


class Login(View, TemplateResponseMixin):
    """
    Class for handling login functionality
    """
    template_name = 'login.html'
    form = LoginForm

    def get(self, request):
        # Checking if user already logged in
        if request.user.is_authenticated:
            return redirect(reverse('accounts:dashboard'))

        form = self.form()
        return self.render_to_response(locals())

    def post(self, request):
        form = self.form(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            # Checking if valid user
            user = authenticate(username=username, password=password)

            if user:
                # Logging in user
                login(request, user)
                if 'next' in request.GET:
                    return redirect(request.GET.get('next', '/'))
                return redirect(reverse('accounts:dashboard'))
            return self.render_to_response({"form": form, 'errors': 'Invalid user credential.'})
        return self.render_to_response({"form": form, 'errors': form.errors})


class Logout(View):
    """
    Class for handling logout
    """

    def get(self, request):
        # Redirecting user to login page if not logged in
        if not request.user.is_authenticated:
            return redirect(reverse('accounts:login'))

        logout(request)
        return redirect(reverse("accounts:login"))
